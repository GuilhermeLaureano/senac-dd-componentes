package br.senac.componentes.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConexaoDB {

    public static void main(String[] args) throws SQLException {
        ConexaoDB conDB = ConexaoDB.getInstance();
        Connection conn = conDB.getConnection();
        System.out.println(conDB.getConnection());
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("Select * from pessoa");
        while (rs.next()) {
            System.out.println(rs.getString("nomePessoa"));
        }
    }

    //Singleton = Design Pattern
    private ConexaoDB() {
    }
    private static ConexaoDB conexao;

    public static ConexaoDB getInstance() {
        if (conexao == null) {
            conexao = new ConexaoDB();
        }
        return conexao;
    }

    private static Connection conn = null;

    public Connection getConnection() {
        try {
            if (conn != null && conn.isClosed() == false) {
                return conn;
            }
        } catch (SQLException ex) {
            //Não fazer nada, tenta conectar novamente
        }
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(
                    new File("./config.properties")));
        } catch (IOException ex) {
            throw new RuntimeException(
                    "Problemas arquivo config.properties "
                    + "consulte o suporte: " + ex.getMessage(), ex);

        }
        String porta = prop.getProperty("portadb");
        String servidor = prop.getProperty("servidordb");
        String usuario = prop.getProperty("usuariodb");
        String senha = prop.getProperty("senhadb");
        String bancoDados = "projeto";
        String url = "jdbc:mysql://" + servidor + ":" + porta + "/" + bancoDados;
        try {
            conn = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException ex) {
            throw new RuntimeException("Problemas na conexão com o Banco de Dados, "
                    + "consulte o suporte: " + ex.getMessage(), ex);
        }
        return conn;
    }
}
