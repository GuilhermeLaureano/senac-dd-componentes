package br.senac.dd.componente.model;

import java.sql.SQLException;

public interface BaseDAO <A ,B>{
    
    public A getPorId(B id) throws SQLException;
    public B inserir(A objeto) throws SQLException;
    public boolean alterar(A objeto) throws SQLException;
    public boolean excluir(B id) throws SQLException;
        
}
