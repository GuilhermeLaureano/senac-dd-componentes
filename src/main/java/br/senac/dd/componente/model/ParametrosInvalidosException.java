/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.componente.model;

/**
 *
 * @author Aluno
 */
public class ParametrosInvalidosException extends RuntimeException {
    
    private Integer numeroError;

    public ParametrosInvalidosException() {
    }

    public ParametrosInvalidosException(String message) {
        super(message);
    }

    public ParametrosInvalidosException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParametrosInvalidosException(Throwable cause) {
        super(cause);
    }

    public ParametrosInvalidosException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ParametrosInvalidosException(Integer numeroError) {
        this.numeroError = numeroError;
    }

    public ParametrosInvalidosException(Integer numeroError, String message) {
        super(message);
        this.numeroError = numeroError;
    }

    public ParametrosInvalidosException(Integer numeroError, String message, Throwable cause) {
        super(message, cause);
        this.numeroError = numeroError;
    }

    public ParametrosInvalidosException(Integer numeroError, Throwable cause) {
        super(cause);
        this.numeroError = numeroError;
    }

    public ParametrosInvalidosException(Integer numeroError, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.numeroError = numeroError;
    }

    
      

}
